//Question no.1's chart

 fetch ('http://localhost:3000/MatchesPlayedPerYear.json')
    .then(val=>val.json())
    .then(data=>{
        const years=Object.keys(data);
        //console.log(years);
        const Matches=Object.values(data);

        Highcharts.chart('Que1',{
            chart: {
                type: 'column'
            },
            title: {
                text: 'Matches Played Per Year'
            },
            xAxis: {
                categories: years
            },
            yAxis: {
                title: {
                    text: 'Number of Matches'
                }
            },
            series: [{
                name: 'Matches',
                data: Matches
            }]
        })
    }) 





//Question no.2's chart

document.addEventListener('DOMContentLoaded', function(){
    const chart=Highcharts.chart('Que2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Won Matches Per Team Per Year'
        },
        xAxis: {
            categories: [
                '2008', '2009', '2010','2011','2012','2013','2014','2015','2016','2017'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Won Matches'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Kolkata Knight Riders',
            data: [6,3,7,8,12,11,7,8,9]
    
        }, {
            name: 'Chennai Super Kings',
            data: [9,8,9,11,10,12,10,10,0,0]
    
        }, {
            name: 'Delhi Daredevils',
            data: [7,10,7,4,11,3,2,5,7,6]
    
        }, {
            name: 'Royal Challengers Bangalore',
            data: [4,9,8,10,8,9,5,8,9,3]
    
        }, {
            name : 'Rajasthan Royals',
            data : [13,6,6,6,7,11,7,7,0,0]
        },{
            name : 'Kings XI Punjab',
            data : [10,7,4,7,8,8,12,3,4,7]
        },{
            name : 'Deccan Chargers',
            data : [2,9,8,6,4,0,0,0,0]
        },{
            name : 'Mumbai Indians',
            data : [7,5,11,10,10,13,7,10,7,12]
        }]
    })
})


//Q.3: 


fetch('http://localhost:3000/ExtraRunsPerTeamIn2016.json')
.then(val=>val.json())
.then(data=>{
    const Team=Object.keys(data);
    const ExtraRuns=Object.values(data);

    Highcharts.chart('Que3',{
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Extra Runs Per Team'
        },
        xAxis: {
            categories: Team
        },
        yAxis: {
            title: {
                text: 'Extra Runs'
            }
        },
        series: [{
            name: 'Runs',
            data: ExtraRuns
        }]
    })

});





//Q.4


fetch('http://localhost:3000/Top10EconomicalBowlersIn2015.json')
    .then(val=>val.json())
    .then(data=>{
        console.log(data);
        const EcoBowler=Object.keys(data);
        
        const BowlerEconomy=Object.values(data);

        Highcharts.chart('Que4',{
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 10 Economical Bowler In 2015'
            },
            xAxis: {
                categories: EcoBowler
            },
            yAxis: {
                title: {
                    text: 'Economy of Bowlers'
                }
            },
            series: [{
                name: 'Economy',
                data: BowlerEconomy
            }]
        })
        
    })


