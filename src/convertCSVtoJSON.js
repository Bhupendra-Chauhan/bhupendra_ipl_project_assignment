import csvtojson from "csvtojson";
import csv from 'csvtojson'
import fs from 'fs'
import path from 'path'
import {fileURLToPath} from 'url'

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const matchesFilePath = './data/matches.csv'
const deliveriesFilePath = './data/deliveries.csv'

const modifyFilePath = filePath => filePath.replace('.csv', '.json')

async function convertCsvToJson(csvFilePath){
    const csvData = await csv().fromFile(path.resolve(__dirname,csvFilePath))
    const outFilePath = modifyFilePath(csvFilePath)

    fs.writeFileSync(path.resolve(__dirname, outFilePath), JSON.stringify(csvData))

}
convertCsvToJson(matchesFilePath)
convertCsvToJson(deliveriesFilePath)
