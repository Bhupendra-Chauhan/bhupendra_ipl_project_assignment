import path from 'path'
import fs from 'fs'
import {fileURLToPath} from 'url'
import ipl from './ipl.js'


const matchesJSONFilePath = '../data/matches.json';
const deliveriesJSONFilePath = '../data/deliveries.json';
const __dirname=path.dirname(fileURLToPath(import.meta.url));

const matchesJsonData=JSON.parse(fs.readFileSync(path.resolve(__dirname,matchesJSONFilePath)));
const deliveriesJsonData = JSON.parse(fs.readFileSync(path.resolve(__dirname,deliveriesJSONFilePath)));

/* console.log(matchesJsonData[1]);
console.log(deliveriesJsonData[0]); */


const Output_Data = (FileName, JsonData)=>{
    const outputFolderPath = '../public/output';
    const outputFilePath = path.resolve(__dirname,outputFolderPath,`${FileName}.json`);
    fs.writeFileSync(outputFilePath,JSON.stringify(JsonData), "utf-8", (err)=>{
        if(err){
            console.log(err);
        }
    });
}

const MatchesPlayedPerYear=ipl.MatchesPlayedPerYear(matchesJsonData);
const WonMatchesPerTeamPerYear=ipl.WonMatchesPerTeamPerYear(matchesJsonData);
const ExtraRunsPerTeamIn2016=ipl.ExtraRunsPerTeamIn2016(matchesJsonData,deliveriesJsonData,2016);
const Top10EconomicalBowlersIn2015=ipl.Top10EconomicalBowlersIn2015(matchesJsonData,deliveriesJsonData,2015);


Output_Data("MatchesPlayedPerYear", MatchesPlayedPerYear);
Output_Data("WonMatchesPerTeamPerYear", WonMatchesPerTeamPerYear);
Output_Data("ExtraRunsPerTeamIn2016",ExtraRunsPerTeamIn2016);
Output_Data("Top10EconomicalBowlersIn2015", Top10EconomicalBowlersIn2015);


