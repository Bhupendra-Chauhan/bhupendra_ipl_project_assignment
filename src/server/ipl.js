//Q1.Number of matches played per year for all the years in IPL.

const MatchesPlayedPerYear =(matchesFile) =>{
    const numberOfMatchesPerYear = matchesFile.reduce((matches,match)=>{
        const year=match.season;
        if(matches[year]){
            matches[year] += 1;
        }
        else{
            matches[year] = 1;
        }
        return matches;
    },{})
    return numberOfMatchesPerYear;
} 



//Q2. Number of matches won per team per year in IPL.

const WonMatchesPerTeamPerYear=(matchesFile)=>{
    const matchesWon=matchesFile.reduce(function(matches,match){
        const year=match.season;
        if(matches[year]){
            if(matches[year][match.winner]){
                matches[year][match.winner]+=1;
            }
            else if(!matches[year][match.winner] && matches[year][match.winner]!==''){
                matches[year][match.winner]=1;
            }
            return matches;
        }
        else{
            matches[year]={};
            matches[year][match.winner]=1;
        }
        return matches;

    },{})
    return matchesWon;
}




//Q.3   Extra runs conceded per team in the year 2016

const ExtraRunsPerTeamIn2016=(matchesFile,deliveriesFile,year)=>{
    const match_ids=matchesFile.filter(match=>{              //it will give the match ids of all matches played in 2016
        if(match.season==year){
            return match.id;
        }
    }).map(match=>parseInt(match.id));

    const deliveries=deliveriesFile.filter(delivery=>{ //it will give all deliveries that are played with fetched match_id       
        if(match_ids.includes(parseInt(delivery.match_id))){
            return delivery;
        }
    })

    let ExtraRun=deliveries.reduce(function(run,del){        
        if(run[del.bowling_team]){
            run[del.bowling_team]+= +(del.extra_runs);
            //run[del.bowling_team]+= parseInt(del.extra_runs);
        }
        else{
            run[del.bowling_team]= +del.extra_runs;
        }
        return run;

    },{})
    return ExtraRun;
}




//Q.4: Top 10 economical bowlers in the year 2015


let Top10EconomicalBowlersIn2015 = (matchesFile,deliveriesFile,year)=>{
    let match_ids=matchesFile.filter(match=>{
        if(match.season==year){
            return match.id;
        }
    }).map(match=>parseInt(match.id));


    let deliveries_2015=deliveriesFile.filter(delivery=>{
        if(match_ids.includes(parseInt(delivery.match_id))){
            return delivery;
        }
    })
    
    deliveries_2015=deliveries_2015.sort((a,b)=>{
        if(a.bowler<b.bowler){
            return 1;
        }
        else{
            return -1;
        }
    })

    let economicalBowlers=deliveries_2015.reduce(function(deliveries,delivery){
        const len=deliveries.length;
        const Bowler=delivery.bowler;
        if(len>0 && Bowler== deliveries[len-1].bowler){
            deliveries[len-1].total_runs+= +(delivery.total_runs);
            deliveries[len-1].bowls+=1;
            deliveries[len-1].Economy=(deliveries[len-1].total_runs/(deliveries[len-1].bowls/6)).toFixed(2);
        }
        else{
            deliveries.push({bowler : Bowler, total_runs : parseInt(delivery.total_runs), bowls : 1, Economy : 0});
        }
        return deliveries;
    },[])
    //console.log(economicalBowlers)
    economicalBowlers=economicalBowlers.sort((a,b)=>{
        if(parseInt(a.Economy*100)<parseInt(b.Economy*100)){
            return -1;
        }
        else{
            return 1;
        }
    })
    //console.log(economicalBowlers)
    economicalBowlers=economicalBowlers.filter((e,i)=> i<10);
    economicalBowlers=economicalBowlers.reduce((Bowler,Eco)=>{
        if(Bowler[Eco.bowler]=Number(Eco.Economy))
            return Bowler;
            else
            return {}

    },{})
    return economicalBowlers;


} 





export default {
    MatchesPlayedPerYear,
    WonMatchesPerTeamPerYear,
    ExtraRunsPerTeamIn2016,
    Top10EconomicalBowlersIn2015
}