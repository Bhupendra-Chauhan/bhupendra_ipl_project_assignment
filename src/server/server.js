import http from 'http'
import fs from 'fs'
import path from 'path'
import {fileURLToPath} from 'url'

const __dirname=path.dirname(fileURLToPath(import.meta.url))
//console.log(__dirname);

const server=http.createServer((req,res)=>{
    const reqUrl=req.url;
    console.log("requrl is: ",reqUrl);
    //const reqUrlFileExt=path.extname(reqUrl);
    const reqUrlFileExtFun = (reqUrl) => {
        if(req.url === '/')
        {
            return '/';
        } else {
            return path.extname(req.url);
        }
    }
    let reqUrlFileExt = reqUrlFileExtFun(reqUrl);

    if(reqUrlFileExt === '.html'){
        reqUrlFileExt='/';
    } 

    switch(reqUrlFileExt){
        case '/':
            const htmlFilePath=path.join(__dirname,"../client/index.html");
            fs.readFile(htmlFilePath,"utf-8",(err,htmlData)=>{
                if(err){
                    res.writeHead(404);
                    console.log(err)
                }
                else{
                    res.writeHead(200,{
                        'Content-Type':'text/html'
                    })
                    res.end(htmlData)
                }
            })
            break;

            case '.js':
                const jsFilePath=path.join(__dirname,"../client",reqUrl);
                fs.readFile(jsFilePath,"utf-8",(err,jsData)=>{
                    if(err){
                        res.writeHead(404)
                        console.log(err)
                    }
                    else{
                        res.writeHead(200,{
                            'Content-Type' : "application/javascript"
                        })
                        res.end(jsData)
                    }
                })
                break;


                case '.json':
                    const jsonFilePath=path.join(__dirname,"../public/output",reqUrl);
                    fs.readFile(jsonFilePath,"utf-8",(err,jsonData)=>{
                        if(err){
                            res.writeHead(404);
                            console.log(err);
                        }
                        else{
                            res.writeHead(200,{
                                'Content-Type' : "application/json"
                            })
                            res.end(jsonData)
                        }
                    })
                    break;
    }
});
server.listen(3000,()=>{console.log("Welcome to Bhupendra's world")});